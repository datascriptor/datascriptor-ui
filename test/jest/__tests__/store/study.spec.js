import study from 'src/store/study';
import {
    STUDY_CREATED, STUDY_CONVERTED_SUCCESS, DESIGN_COMPUTED_SUCCESS
} from '../../../../src/store/mutation-types';

let testState;

beforeEach(() => {
    testState = {
        studyData: {
            design: {},
            dataset: {}
        },
        studyReport: {
            arms: {},
            events: {}
        }
    };
});

describe('study', () => {
    describe('getters', () => {
        describe('studyData', () => {
            test('gets the \'studyData\' property from the state', () => {
                expect(study.getters.studyData(testState)).toStrictEqual(testState.studyData);
            });
        });
        describe('studyReport', () => {
            test('gets the \'studyReport\' property from the state', () => {
                expect(study.getters.studyReport(testState)).toStrictEqual(testState.studyReport);
            });
        });
    });

    describe('mutations', () => {
        test('STUDY_CREATED', () => {
            const testStudy = { design: {}, dataset: [] };
            study.mutations[STUDY_CREATED](testState, { studyData: testStudy });
            expect(testState.studyData).toStrictEqual(testStudy);
        });
        // TODO implement
        test('STUDY_CONVERTED_SUCCESS', () => {});
        test('DESIGN_COMPUTED_SUCCESS', () => {});
    });

    describe('actions', () => {
        // TODO implement
        describe('setStudy', () => {
            test('it correcly sets the study propety in the store', () => {});
        });
    });
});
