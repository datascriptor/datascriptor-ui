import StudyMetadataForm from 'src/pages/StudyMetadataForm';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import { components } from '../utils';
import { Quasar } from 'quasar';
import Vuex from 'vuex';

import testStudyReport from '../../../fixtures/test-study-report';
import testStudy from '../../../fixtures/test-study';

describe('StudyMetadataForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let emptyStore, populatedStore;

    beforeEach(() => {
        emptyStore = new Vuex.Store({
            getters: {
                // TODO investigate if this is the correct way of setting getters in the store.
                'study/computedStudyDesign': () => { return {}; },
                'study/studyData': () => { return {}; }
            }
        });
        populatedStore = new Vuex.Store({
            getters: {
                // TODO investigate if this is the correct way of setting getters in the store.
                'study/computedStudyDesign': () => testStudy.design,
                'study/studyData': () => { return {}; }
            }
        });
    });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof StudyMetadataForm.data).toBe('function');
            const defaultData = StudyMetadataForm.data();
            expect(defaultData).toHaveProperty('studyDesignTypes');
            expect(defaultData).toHaveProperty('form.name', '');
            expect(defaultData).toHaveProperty('form.description', '');
        });
    });

    describe('computed', () => {
        describe('hasComputedStudyDesign', () => {
            test('false if study design is empty', () => {
                const wrapper = shallowMount(StudyMetadataForm, { localVue, store: emptyStore });
                expect(wrapper.vm.hasComputedStudyDesign).toBe(false);
            });

            test('true if study design is empty', () => {
                const wrapper = shallowMount(StudyMetadataForm, { localVue, store: populatedStore });
                expect(wrapper.vm.hasComputedStudyDesign).toBe(true);
            });
        });
    });

    describe('methods()', () => {
        describe('onSubmit()', () => {});
        describe('onReset()', () => {});
    });
});
