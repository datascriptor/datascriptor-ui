import StudyJson from 'src/pages/StudyJson';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import { components } from '../utils';
import { Quasar } from 'quasar';

describe('StudyJson', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof StudyJson.data).toBe('function');
            const defaultData = StudyJson.data();
            expect(defaultData.files).toStrictEqual([]);
        });
    });

    describe('computed', () => {
        describe('uploadDisabled', () => {
            test('uploadDisabled is true when no file has been selected', () => {
                const localThis = { files: [] };
                expect(StudyJson.computed.uploadDisabled.call(localThis)).toBe(true);
            });
            test('uploadDisabled is false when a file has been selected', () => {
                const localThis = { files: ['/path/to/some/file.json'] };
                expect(StudyJson.computed.uploadDisabled.call(localThis)).toBe(false);
            });
        });
    });

    describe('methods', () => {
        let wrapper;

        describe('addJSONFile', () => {
            beforeEach(() => {
                wrapper = shallowMount(StudyJson, {
                    localVue
                });
                console.log(`Wrapper HTML: ${wrapper.html()}`);
            });

            test('add a JSON file to be sent to the server', () => {
                const dataTransfer = {
                    files: ['/path/to/filename.ext']
                };
                wrapper.find('div.dropzone').trigger('drop', {
                    dataTransfer
                });
                expect(wrapper.vm.$data.files).toStrictEqual(dataTransfer.files);
            });
        });

        describe('highlight', () => {
            beforeEach(() => {
                wrapper = shallowMount(StudyJson, { localVue });
            });

            test('highlight the dropzone on dragenter', () => {
                const dropzone = wrapper.find('div.dropzone');
                dropzone.trigger('dragenter');
                expect(dropzone.classes()).toContain('highlight');
            });
            test('highlight the dropzone on dragover', () => {
                const dropzone = wrapper.find('div.dropzone');
                dropzone.trigger('dragover');
                expect(dropzone.classes()).toContain('highlight');
            });
        });

        describe('unhighlight', () => {
            beforeEach(() => {
                wrapper = shallowMount(StudyJson, { localVue });
            });

            test('unhighlight the dropzone on dragleave', () => {
                const dropzone = wrapper.find('div.dropzone');
                dropzone.trigger('dragleave');
                expect(dropzone.classes()).not.toContain('highlight');
            });
        });

        describe('removeFile', () => {
            test('remove the file from the list', () => {});
        });
    });
});
