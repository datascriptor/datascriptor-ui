import StudyJsonUploader from 'src/pages/StudyJsonUploader';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import { components } from '../utils';
import { Quasar } from 'quasar';
import Vuex from 'vuex';

import testStudyReport from '../../../fixtures/test-study-report';
import testStudy from '../../../fixtures/test-study';

describe('StudyJsonUploader', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let emptyStore, populatedStore, actions;

    beforeEach(() => {
        actions = {
            'study/storeConvertedStudy': jest.fn()
        };
        emptyStore = new Vuex.Store({
            getters: {
                // TODO investigate if this is the correct way of setting getters in the store.
                'study/studyData': () => { return {}; },
                'study/studyReport': () => { return {}; }
            },
            actions
        });
        populatedStore = new Vuex.Store({
            getters: {
                // TODO investigate if this is the correct way of setting getters in the store.
                'study/studyData': () => testStudy,
                'study/studyReport': () => testStudyReport
            }
        });
    });

    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof StudyJsonUploader.data).toBe('function');
            const defaultData = StudyJsonUploader.data();
            expect(defaultData).toHaveProperty('url');
            expect(defaultData.edit).toBe(false);
            expect(defaultData.maximizedToggle).toBe(true);
        });
    });

    describe('computed', () => {
        // const wrapper = shallowMount(StudyJsonUploader, { localVue, store });

        describe('reportText', () => {
            test('returns null if no studyReport is available', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: emptyStore });
                expect(wrapper.vm.reportText).toBe(null);
            });

            test('computes the textual report for the study design', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: populatedStore });
                for (const elemText of Object.values(testStudyReport.elements)) {
                    expect(wrapper.vm.reportText).toMatch(elemText);
                }
            });
        });

        describe('studyDesign', () => {
            test('returns an empty object if no studyDesign is available', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: emptyStore });
                expect(wrapper.vm.studyDesign).toStrictEqual({});
            });

            test('returns the studyDesign if available', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: populatedStore });
                expect(wrapper.vm.studyDesign).toStrictEqual(testStudy.design);
            });
        });

        describe('hasStudyDesign', () => {
            test('returns an false object if no studyDesign is available', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: emptyStore });
                expect(wrapper.vm.hasStudyDesign).toStrictEqual(false);
            });

            test('returns true if the studyDesign if available', () => {
                const wrapper = shallowMount(StudyJsonUploader, { localVue, store: populatedStore });
                expect(wrapper.vm.hasStudyDesign).toStrictEqual(true);
            });
        });
    });

    describe('methods()', () => {
        describe('showConverted()', () => {
            const info = {
                xhr: { responseText: '{}' }
            };
            let wrapper;

            beforeEach(() => {
                wrapper = shallowMount(StudyJsonUploader, {
                    localVue,
                    store: emptyStore
                });
            });

            test('it should store the result received from the server into the text field', () => {
                expect(wrapper).not.toBe(null);
                wrapper.vm.showConverted(info);
                expect(actions['study/storeConvertedStudy']).toHaveBeenCalled();
            });
        });
    });
});
