import MermaidRenderer from 'src/components/MermaidRenderer';
import { shallowMount } from '@vue/test-utils';
import testStudy from '../../../fixtures/test-study';
import { mermaidAPI } from 'mermaid';

jest.mock('mermaid');

describe('MermaidRenderer', () => {
    describe('data()', () => {
        test('the data() is initialised correctly', () => {
            expect(typeof MermaidRenderer.data).toBe('function');
            const defaultData = MermaidRenderer.data();
            expect(defaultData.elementPlanSvg).toBeNull();
            expect(defaultData).toHaveProperty('config');
        });
    });

    describe('computed', () => {
        describe('elementPlanGraph', () => {
            test('returns the Mermaid graph the treatment and non-treatment elements', () => {
                const wrapper = shallowMount(MermaidRenderer, {
                    propsData: {
                        studyDesign: testStudy.design
                    }
                });
                expect(mermaidAPI.render).toHaveBeenCalled();
                expect(wrapper.vm.elementPlanGraph).toMatch('graph LR');
                for (const arm of testStudy.design.arms) {
                    if (arm.elements) {
                        for (const element of arm.elements) {
                            expect(wrapper.vm.elementPlanGraph).toMatch(element.name);
                        }
                    }
                }
            });
        });
    });

    describe('mounted()', () => {

    });
});
