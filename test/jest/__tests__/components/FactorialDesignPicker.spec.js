import FactorialDesignPicker from 'src/components/FactorialDesignPicker';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { Quasar } from 'quasar';
import { components } from '../utils';
import testStudy from '../../../fixtures/test-study';
import testStudyExpanded from '../../../fixtures/test-study-expanded';

describe('StudyMetadataForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof FactorialDesignPicker.data).toBe('function');
            const { design } = testStudy;
            const wrapper = shallowMount(FactorialDesignPicker, {
                localVue,
                propsData: {
                    studyDesign: design
                }
            });
            expect(wrapper.vm.$data).toStrictEqual({
                selectedArms: Array(design.arms.length).fill(false)
            });
        });
    });

    describe('computed', () => {
        describe('expandedStudyDesign', () => {
            test('it computes the expanded JSON of the current Study Design object', () => {
                const wrapper = shallowMount(FactorialDesignPicker, {
                    localVue,
                    propsData: {
                        studyDesign: testStudy.design
                    }
                });
                expect(wrapper.vm.expandedStudyDesign).toStrictEqual(testStudyExpanded.design);
            });
        });
    });
});
