import axios from 'axios';

async function generateStudyDesign(payload) {
    const response = await axios.post('/api/studyDesign/generate', payload);
    // FIXME: extract the body from the response
    return response.data;
}

export default {
    generateStudyDesign
};
