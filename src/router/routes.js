
const routes = [
    {
        path: '/',
        component: () => import('layouts/MyLayout.vue'),
        children: [
            {
                path: '', component: () => import('pages/DataSubmission.vue')
            },
            {
                path: '/metadataForm', component: () => import('pages/StudyMetadataForm.vue')
            },
            {
                path: '/jsonSubmission', component: () => import('pages/StudyJson.vue')
            },
            {
                path: '/jsonSubmissionAlt', component: () => import('pages/StudyJsonUploader.vue')
            }
        ]
    }
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        component: () => import('pages/Error404.vue')
    });
}

export default routes;
