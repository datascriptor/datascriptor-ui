import {
    STUDY_CREATED, STUDY_CONVERTED_SUCCESS, DESIGN_COMPUTED_SUCCESS
} from './mutation-types';

const state = {
    studyData: {},
    studyReport: {},
    computedDesign: {}
};

const getters = {
    studyData: state => state.studyData,
    studyReport: state => state.studyReport,
    studyDesign: state => {
        const { studyData: { design = {} } } = state;
        return design;
    },
    computedStudyDesign: state => state.computedDesign
};

const mutations = {
    [STUDY_CREATED](state, { studyData }) {
        state.studyData = studyData;
    },
    [STUDY_CONVERTED_SUCCESS](state, { studyData = {}, studyReport = {} }) {
        state.studyData = studyData;
        state.studyReport = studyReport;
    },
    [DESIGN_COMPUTED_SUCCESS](state, { studyDesign }) {
        state.computedDesign = studyDesign;
    }
};

const actions = {

    setStudy({ commit, state }, { study }) {
        commit(STUDY_CREATED, {
            studyData: study
        });
    },

    storeConvertedStudy({ commit, state }, payload) {
        commit(STUDY_CONVERTED_SUCCESS, {
            studyData: payload.studyData,
            studyReport: payload.studyReport
        });
    },

    setComputedStudyDesign({ commit, state }, { studyDesign }) {
        commit(DESIGN_COMPUTED_SUCCESS, {
            studyDesign
        });
    }
};

export default {
    namespaced: true,
    getters,
    mutations,
    actions,
    state
};
